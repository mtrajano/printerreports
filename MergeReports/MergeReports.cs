﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace MergeReports
{
    class MergeReports
    {
        static void Main(string[] args)
        {
            string allText = "";
            for (int i = 1; i <= 3; i++)
            {
                try
                {
                    allText += String.Format("Computador {0}\n", i);
                    allText += File.ReadAllText(publicPath + String.Format("\\comp{0}.txt", i));
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine("Not all files ready yet");
                }
            }

            EmailResults(allText);

            for (int i = 1; i <= 3; i++)
            {
                File.Delete(publicPath + String.Format("\\comp{0}.txt", i));
            }
        }

        static void EmailResults(string message)
        {
            const string fromAddress = "";
            const string fromPassword = "";
            const string toAddress = "";
            const string subject = "Resultados da Impressora";

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromAddress);
            mail.To.Add(toAddress);
            mail.Subject = subject;
            mail.Body = message;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress, fromPassword)
            };

            smtp.Send(mail);
        }
    }
}

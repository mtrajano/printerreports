﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PrinterLog
{
    class PrinterLog
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> simplerName = new Dictionary<string, string>()
            {
                { @"https://www.pontualmt.net/Adminsite/index.cfm?fuseaction=confirmation&no_header&no_footer", "Pontual Fechamento" },
                { @"http://174.129.200.48/currencyreports/rptserver.asp", "Rana Fechamento" },
                { @"http:\/\/174\.129\.200\.48\/remessa\/ReciboDuplo2\.asp\?rmid=\d+" , "Rana Recibo"},
                { @"Microsoft Word - Fechamento Philipe", "Fechamento do Philipe" },
                { @"Microsoft Word - Ana fechamento", "Fechamento da Ana" },
                { @"Microsoft Word - Fechamento Dayane", "Fechamento da Dayane" },
            };

            string body = "";
            string eventID = "307";
            string eventPath = "Microsoft-Windows-PrintService/Operational";
            string sQuery = String.Format("*[System/EventID={0}]", eventID);

            var elQuery = new EventLogQuery(eventPath, PathType.LogName, sQuery);
            var elReader = new EventLogReader(elQuery);

            body += String.Format("Horario,Nome,Paginas\r\n");

            for (EventRecord eventInstance = elReader.ReadEvent(); eventInstance != null; eventInstance = elReader.ReadEvent())
            {
                DateTime eventTime = eventInstance.TimeCreated ?? DateTime.MinValue;
                string tempName = getDocumentName(eventInstance.FormatDescription());
                string eventName;

                //if not in dic use regular name
                if (!simplerName.TryGetValue(tempName, eventName))
                {
                    eventName = tempName;
                }
                var otherMatches = (from result in simplerName
                                    where Regex.Match(eventName, result.Key).Success
                                    select result.Value).FirstOrDefault();
                eventName = otherMatches ?? eventName;
                int eventPages = getNumPages(eventInstance.FormatDescription());
                body += String.Format("{0},{1},{2}\r\n", eventTime, eventName, eventPages);
            }

            string publicPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
            using (StreamWriter outfile = new StreamWriter(publicPath + @"\comp1.txt"))
            {
                outfile.Write(body);
            }

            Console.WriteLine(body);

            ClearLog(eventPath);
        }

        public static void ClearLog(string epath)
        {
            var psi = new ProcessStartInfo(
                "wevtutil.exe",
                String.Format("cl {0}", epath));
            psi.Verb = "runas";

            using (var p = new Process())
            {
                p.StartInfo = psi;
                p.Start();
            }
        }

        public static string getDocumentName(string info)
        {
            string docNum = Regex.Match(info, @"Document \d+").Value;

            int firstIndex = info.IndexOf(docNum + ", ");
            int lastIndex = info.IndexOf(" owned by");

            return info.Substring(firstIndex + docNum.Length + ", ".Length, lastIndex - (firstIndex + docNum.Length + ", ".Length));
        }

        public static int getNumPages(string info)
        {
            int firstIndex = info.IndexOf("Pages printed: ");
            int lastIndex = info.IndexOf(". No user action is required");

            string numString = info.Substring(firstIndex + "Pages printed: ".Length, lastIndex - (firstIndex + "Pages printed: ".Length));

            return Int32.Parse(numString);
        }
    }
}
